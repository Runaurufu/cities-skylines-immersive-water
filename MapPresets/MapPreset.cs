﻿using ColossalFramework;
using Runaurufu.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;

namespace Runaurufu.ImmersiveWater
{
  [Serializable]
  public class MapPreset
  {
    internal readonly string MapPresetDirectoryPath = Path.Combine(AssemblyConfig.GameDirectoryPath, "MapPresets");

    public void PopulateFromCurrentGameState()
    {
      SimulationMetaData metaData = Singleton<SimulationManager>.instance.m_metaData;

      //TODO Find some way to populate map name (preset code?)
      //this.Environment = metaData.m_environment;

      FastList<WaterSource> waterSources = Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources;
      List<PresetWaterSourceData> defaultWaterSources = new List<PresetWaterSourceData>();

      for (int i = 0; i < waterSources.m_size; i++)
      {
        if (waterSources.m_buffer[i].m_type == WaterSource.TYPE_NATURAL)
        {
          defaultWaterSources.Add(new PresetWaterSourceData()
          {
            Position = waterSources.m_buffer[i].m_inputPosition,

            DefaultLevel = new WaterSourceState()
            {
              Target = waterSources.m_buffer[i].m_target,
              InputRate = waterSources.m_buffer[i].m_inputRate,
              OutputRate = waterSources.m_buffer[i].m_outputRate,
            },
          });
        }
      }

      this.DefaultWaterSources = defaultWaterSources.ToArray();
    }

    public void Save()
    {
      string presetFilePath = Path.Combine(MapPresetDirectoryPath, this.PresetCode + ".xml");
      try
      {
        XmlSerializer serializer = new XmlSerializer(typeof(MapPreset));

        XmlWriterSettings settings = new XmlWriterSettings();
        settings.Indent = true;

        using (XmlWriter xmlWriter = XmlWriter.Create(presetFilePath, settings))
          serializer.Serialize(xmlWriter, this);
      }
      catch (Exception ex)
      {
      }
    }

    public String PresetName { get; set; }

    /// <summary>
    /// It must be unique map preset code!
    /// </summary>
    public String PresetCode { get; set; }

    public PresetWaterSourceData[] DefaultWaterSources;
    public PresetWaterSourceData[] ExtendedWaterSources;
    public float DefaultSeaLevel { get; set; }

  }

  [Serializable]
  public class PresetWaterSourceData
  {
    public Vector3 Position;
    public WaterSourceState LowLevel;
    public WaterSourceState DefaultLevel;
    public WaterSourceState HighLevel;
    public WaterSourceType SourceType;

    public string Complex
    {
      set { }
      get
      {
        return "new PresetWaterSourceData()"
          + Environment.NewLine + "          {" + string.Format(@"
            SourceType = WaterSourceType.Unknown,
            Position = new UnityEngine.Vector3({0}f, {1}f, {2}f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState({3}, {4}, {5}),
            HighLevel = new WaterSourceState(0, 0, 0),",
            this.Position.x, this.Position.y, this.Position.z,
            this.DefaultLevel.Target, this.DefaultLevel.InputRate, this.DefaultLevel.OutputRate)
         + Environment.NewLine + "          },";
      }
    }
  }

  [Serializable]
  public struct WaterSourceState
  {
    public ushort Target;
    public uint InputRate;
    public uint OutputRate;

    /// <summary>
    /// Return true if any of values is set to non-zero value.
    /// </summary>
    public bool IsSet
    {
      get { return this.Target != 0 || this.InputRate != 0 || this.OutputRate != 0; }
    }

    public WaterSourceState(ushort target, uint inputRate, uint outputRate)
    {
      this.Target = target;
      this.InputRate = inputRate;
      this.OutputRate = outputRate;
    }

    public override string ToString()
    {
      return string.Format("(Target:{0}, InRate: {1}, OutRate: {2})", this.Target, this.InputRate, this.OutputRate);
    }
  }

  public enum WaterSourceType : byte
  {
    /// <summary>
    /// I do not know what kind of source it is...
    /// </summary>
    Unknown = 0,
    /// <summary>
    /// It is out of the map river source.
    /// </summary>
    River = 1,
    /// <summary>
    /// It is source of water coming from within the ground.
    /// </summary>
    Spring = 2,
    /// <summary>
    /// It is place where ground water of surrounding area flows to.
    /// </summary>
    Pond = 3,
    /// <summary>
    /// It is a pond which tends to provide constant output of water (creates of a river).
    /// </summary>
    Lake = 4,
    /// <summary>
    /// This source acts as infinite reinforcement for sea water to maintain steady level in areas far from map border.
    /// </summary>
    Sea = 5,
    /// <summary>
    /// This source acts as infinite supply of water for filling enormously big bodies of water.
    /// </summary>
    BodyOfWater = 6,
  }
}
