﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Runaurufu.ImmersiveWater
{
  internal sealed class MapPresets
  {
    public static readonly MapPreset[] AllPresets =
    {
      #region Black Woods
      new MapPreset
      {
        PresetCode = "VANILLA_BLACK_WOODS",
        PresetName = "Black Woods",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8278.626f,79.10923f,7313.851f),
            LowLevel = new WaterSourceState(5845, 19550, 19550),
            DefaultLevel = new WaterSourceState(8305,32767,32767),
            HighLevel = new WaterSourceState(8616, 69903, 69903),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8303.425f,78.3083954f,-1612.8999f),
            LowLevel = new WaterSourceState(5330, 39900, 39900),
            DefaultLevel = new WaterSourceState(7617, 64224, 64224),
            HighLevel = new WaterSourceState(7617, 69903, 69903),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-4576.3667f,37.38695f,-8354.792f),
            LowLevel = new WaterSourceState(8000, 5904, 5904),
            DefaultLevel = new WaterSourceState(12217, 64224, 64224),
            HighLevel = new WaterSourceState(14441, 287383, 287383),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-866.080444f,57.37513f,8150.155f),
            LowLevel = new WaterSourceState(4400, 35660, 35660),
            DefaultLevel = new WaterSourceState(5426, 65535, 65535),
            HighLevel = new WaterSourceState(6189, 427191, 427191),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8366.137f,74.02366f,-2358.651f),
            LowLevel = new WaterSourceState(5900, 35660, 35660),
            DefaultLevel = new WaterSourceState(7954, 64224, 64224),
            HighLevel = new WaterSourceState(7888, 225246, 225246),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Pond,
            Position = new UnityEngine.Vector3(3118.25879f,46.47216f,6516.73975f),
            LowLevel = new WaterSourceState(3200,16767,16767),
            DefaultLevel = new WaterSourceState(4600,32767,32767),
            HighLevel = new WaterSourceState(5220,64767,64767),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Lake,
            Position = new UnityEngine.Vector3(-7982.96533f,62.6891327f,2547.321f),
            LowLevel = new WaterSourceState(6050, 11571, 11571),
            DefaultLevel = new WaterSourceState(7151, 65535, 65535),
            HighLevel = new WaterSourceState(7160, 69903, 69903),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Sea,
            Position = new UnityEngine.Vector3(7931.76025f,38.9013977f,7641.195f),
            LowLevel = new WaterSourceState(5689, 32191, 32191),
            DefaultLevel = new WaterSourceState(5689,65535,65535),
            HighLevel = new WaterSourceState(5689, 427191, 427191),
          },
        },
        DefaultSeaLevel = 57.0f,
      },
      #endregion

      #region Cliffside Bay
      new MapPreset
      {
        PresetCode = "VANILLA_CLIFFSIDE_BAY",
        PresetName = "Cliffside Bay",
        DefaultWaterSources = new  PresetWaterSourceData[]
        { },
        DefaultSeaLevel = 40,
      },
      #endregion

      #region Diamond Coast X
      new MapPreset
      {
        PresetCode = "VANILLA_DIAMOND_COAST",
        PresetName = "Diamond Coast",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Pond,
            Position = new UnityEngine.Vector3(2148.104f, 58.47491f, 2785.5f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6942, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Lake,
            Position = new UnityEngine.Vector3(683.6678f, 79.24931f, 5679.046f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8271, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8261.911f, 79.25f, 7666.238f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8271, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(7906.315f, 79.25207f, 8420.876f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8272, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8330.792f, 79.22873f, -1661.88f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8270, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 60,
      },
      #endregion

      #region Foggy Hills
      new MapPreset
      {
        PresetCode = "VANILLA_FOGGY_HILLS",
        PresetName = "Foggy Hills",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8022.045f, 126.4058f, 8045.555f),
            LowLevel = new WaterSourceState(14100, 10500, 10500),
            DefaultLevel = new WaterSourceState(15547, 32767, 32767),
            HighLevel = new WaterSourceState(16000, 80000, 80000),
          },
        },
        DefaultSeaLevel = 119.81f,
      },
      #endregion

      #region Grand River X
      new MapPreset
      {
        PresetCode = "VANILLA_GRAND_RIVER",
        PresetName = "Grand River",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(8369.656f, 78.29555f, -4750.976f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(9038, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(5150.124f, 96.37514f, -8342.426f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(11662, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 81.27f,
      },
      #endregion

      #region Green Plains X
      new MapPreset
      {
        PresetCode = "VANILLA_GREEN_PLAINS",
        PresetName = "Green Plains",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Lake,
            Position = new UnityEngine.Vector3(-1152.252f, 75.6875f, 4130.563f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8368, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8408.598f, 89.28125f, 3107.489f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8742, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(8320.884f, 89.22013f, 7499.853f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8818, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 45,
      },
      #endregion

      #region Islands X
      new MapPreset
      {
        PresetCode = "VANILLA_ISLANDS",
        PresetName = "Islands",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-3283.94f, 140.9976f, 8057.482f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(16389, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8373.962f, 73.01176f, 5109.616f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7882, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8407.847f, 72.84961f, -878.7308f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7510, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Lake,
            Position = new UnityEngine.Vector3(-2942.823f, 74.96875f, -4698.436f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7775, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(3970.509f, 80.13934f, 8356.088f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8324, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 52,
      },
      #endregion

      #region Lagoon Shore X
      new MapPreset
      {
        PresetCode = "VANILLA_LAGOON_SHORE",
        PresetName = "Lagoon Shore",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(223.0565f, 79.375f, 8313.199f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8472, 9830, 9830),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Spring,
            Position = new UnityEngine.Vector3(-3463.149f, 78.90625f, 6826.703f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8249, 9830, 9830),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Sea,
            Position = new UnityEngine.Vector3(8162.582f, 0f, -2034.725f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(3199, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Lake,
            Position = new UnityEngine.Vector3(1863.104f, 43.89643f, 894.5506f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(9535, 22937, 22937),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 56.34f,
      },
      #endregion

      #region Riverrun X
      new MapPreset
      {
        PresetCode = "VANILLA_RIVERRUN",
        PresetName = "Riverrun",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-7966.438f, 16.92187f, 5903.422f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7399, 51117, 51117),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(2146.464f, 11.24402f, 7582.301f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8093, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Pond,
            Position = new UnityEngine.Vector3(3793.097f, 27.0382f, 1082.37f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(5142, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 40,
      },
      #endregion

      #region Sandy Beach X
      new MapPreset
      {
        PresetCode = "VANILLA_SANDY_BEACH",
        PresetName = "Sandy Beach",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(5151.526f, 58.00098f, 8309.209f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6911, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(7170.94f, 1.086787E-05f, 1618.36f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6870, 32767, 32767),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 82,
      },
      #endregion

      #region Shady Strands X
      new MapPreset
      {
        PresetCode = "VANILLA_SHADY_STRANDS",
        PresetName = "Shady Strands",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8278.137f, 64.03125f, 4260.071f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7052, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(-8222.733f, 64.03125f, 8420.387f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7297, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(-7478.004f, 64.03125f, 8416.453f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7297, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(-6696.372f, 64.0625f, 8424.169f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7299, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(-5960.303f, 64.09375f, 8436.213f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7301, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(-5268.015f, 64.0625f, 8428.375f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7299, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(-4464.955f, 64.03125f, 8413.188f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7297, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(-3761.288f, 64.25f, 8426.331f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7311, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(-3058.206f, 67.93979f, 8436.611f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7548, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(-2432.021f, 67.00622f, 8446.627f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7488, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(-1788.645f, 64.11655f, 8427.016f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7303, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(-1163.154f, 64.06258f, 8436.704f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7299, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(-487.5482f, 64.44321f, 8434.247f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7325, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(194.6711f, 64.03125f, 8412.824f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7297, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(856.6912f, 64.03125f, 8425.675f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7297, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(1531.008f, 64.03125f, 8440.423f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7297, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(2151.421f, 64.03125f, 8424.619f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7297, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(2883.671f, 64.0625f, 8422.935f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7299, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(3506.103f, 64.0625f, 8425.472f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7299, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(4200.326f, 64.0625f, 8438.842f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7299, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(4902.569f, 64.0625f, 8426.577f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7299, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(5568.333f, 64.03125f, 8410.576f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7297, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(6315.651f, 64.03125f, 8414.037f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7297, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(6924.295f, 64.03125f, 8401.232f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7297, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(7680.615f, 64.03125f, 8400.826f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7297, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(8301.62f, 64.03125f, 8421.021f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7297, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Lake,
            Position = new UnityEngine.Vector3(4231.546f, 64.28175f, -706.1379f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(7061, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 45,
      },
      #endregion

      #region Two Rivers X
      new MapPreset
      {
        PresetCode = "VANILLA_TWO_RIVERS",
        PresetName = "Two Rivers",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(8305.326f, 73.03233f, -7677.469f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8381, 13107, 13107),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(8286.383f, 73.08909f, 5897.84f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(8884, 19660, 19660),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Pond,
            Position = new UnityEngine.Vector3(-804.7249f, 0f, -2670.714f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6424, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Pond,
            Position = new UnityEngine.Vector3(-3751.835f, -0.004549433f, -7171.846f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(4346, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 70.61f,
      },
      #endregion

      #region Frosty Rivers X
      new MapPreset
      {
        PresetCode = "DLC_SF_FROSTY_RIVERS",
        PresetName = "Frosty Rivers",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-1398.385f, 47.06305f, 8219.628f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(5887, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Lake,
            Position = new UnityEngine.Vector3(3170.527f, 47.06223f, 6961.628f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6011, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(8377.343f, 50.06026f, 6498.631f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(5846, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(6664.489f, 47.05171f, 8167.207f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(5993, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8410.469f, 47.88108f, 2187.997f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(5801, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8371.096f, 47.04607f, -903.2288f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(5988, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8382.35f, 47.04515f, -3367.918f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(5818, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Lake,
            Position = new UnityEngine.Vector3(2275.362f, 47.06261f, 4092.365f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(5940, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Lake,
            Position = new UnityEngine.Vector3(2974.063f, 47.06282f, 2734.858f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(5841, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 81.22f,
      },
      #endregion

      #region Icy Islands X
      new MapPreset
      {
        PresetCode = "DLC_SF_ICY_ISLANDS",
        PresetName = "Icy Islands",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(7629.111f, 47.21889f, 8241.61f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6664, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(7011.971f, 47.21875f, 8307.501f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6693, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(6300.903f, 47.22065f, 8329.57f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6615, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(8276.369f, 47.41708f, -2276.701f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6725, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(398.8571f, 47.21875f, 8372.866f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6846, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-2256.876f, 47.2187f, 8339.664f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6518, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-7456.129f, 47.21875f, 8066.24f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6931, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Lake,
            Position = new UnityEngine.Vector3(-6513.987f, 54.08025f, 4789.631f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6694, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-8204.281f, 47.21868f, 4490.926f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6652, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Lake,
            Position = new UnityEngine.Vector3(-7788.712f, 47.21871f, 2004.297f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6365, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Spring,
            Position = new UnityEngine.Vector3(-1281.996f, 47.21629f, 5882.792f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6588, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 65,
      },
      #endregion

      #region Snowy Coast X
      new MapPreset
      {
        PresetCode = "DLC_SF_SNOWY_COAST",
        PresetName = "Snowy Coast",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(4213.732f, 36.28125f, -8230.795f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(5856, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(-4908.136f, 36.28125f, -8331.715f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(5171, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Spring,
            Position = new UnityEngine.Vector3(969.3718f, 0f, 3544.321f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(6386, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 69.19f,
      },
      #endregion

      #region Prussian Peaks X
      new MapPreset
      {
        PresetCode = "DLC_ND_PRUSSIAN_PEAKS",
        PresetName = "Prussian Peaks",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(98.72954f, 176.6563f, 8314.763f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(14095, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(8359.398f, 176.6563f, 5112.869f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(14554, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 161.48f,
      },
      #endregion

      #region The Archipelago
      new MapPreset
      {
        PresetCode = "DLC_ND_THE_ARCHIPELAGO",
        PresetName = "The Archipelago",
        DefaultWaterSources = new  PresetWaterSourceData[]
        { },
        DefaultSeaLevel = 164.56f,
      },
      #endregion

      #region The Dust Bowl X
      new MapPreset
      {
        PresetCode = "DLC_ND_THE_DUST_BOWL",
        PresetName = "The Dust Bowl",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(8089.743f, 418.9063f, -8096.397f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(30275, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(3161.176f, 418.9063f, -8147.259f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(30001, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(8142.508f, 418.9063f, -3137.875f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(30190, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(8173.518f, 418.9063f, -5252.899f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(30406, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.BodyOfWater,
            Position = new UnityEngine.Vector3(5508.784f, 418.9063f, -8051.835f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(29152, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Lake,
            Position = new UnityEngine.Vector3(-41.11283f, 418.9063f, -6752.121f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(29523, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 380,
      },
      #endregion

      #region Arid Plains X
      new MapPreset
      {
        PresetCode = "DLC_MT_ARID_PLAINS",
        PresetName = "Arid Plains",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(5900.022f, 202.4718f, 8516.356f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(17157, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Pond,
            Position = new UnityEngine.Vector3(-2752.203f, 272.8942f, 1926.968f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(20204, 13762, 13762),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(5650.918f, 201.9688f, 8493.211f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(17159, 65535, 65535),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 181.98f,
      },
      #endregion

      #region Regal Hills X
      new MapPreset
      {
        PresetCode = "DLC_MT_REGAL_HILLS",
        PresetName = "Regal Hills",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Spring,
            Position = new UnityEngine.Vector3(269.5981f, 387.931f, 826.5385f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(28119, 655, 655),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.River,
            Position = new UnityEngine.Vector3(1297.6f, 295.5081f, 7548.384f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(22174, 1310, 1310),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Spring,
            Position = new UnityEngine.Vector3(1130.924f, 478.9948f, 1782.848f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(34679, 655, 655),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Spring,
            Position = new UnityEngine.Vector3(-1931.559f, 345.9658f, -3424.318f),
            LowLevel = new WaterSourceState(0, 0, 0),
            DefaultLevel = new WaterSourceState(25301, 655, 655),
            HighLevel = new WaterSourceState(0, 0, 0),
          },
        },
        DefaultSeaLevel = 57.47f,
      },
      #endregion

      #region Seven Lakes
      new MapPreset
      {
        PresetCode = "DLC_MT_SEVEN_LAKES",
        PresetName = "Seven Lakes",
        DefaultWaterSources = new  PresetWaterSourceData[]
        {
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Pond,
            Position = new UnityEngine.Vector3(1763.063f, 48.78445f, -2093.976f),
            LowLevel = new WaterSourceState(3525, 2000, 2000),
            DefaultLevel = new WaterSourceState(3783, 6553, 6553),
            HighLevel = new WaterSourceState(4125, 38000, 38000),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Pond,
            Position = new UnityEngine.Vector3(-1841.524f, 44.32643f, -1983.594f),
            LowLevel = new WaterSourceState(3300, 600, 600),
            DefaultLevel = new WaterSourceState(3753, 1310, 1310),
            HighLevel = new WaterSourceState(3800, 27000, 27000),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Pond,
            Position = new UnityEngine.Vector3(-275.0632f, 53.21835f, 216.6364f),
            LowLevel = new WaterSourceState(3575, 4250, 4250),
            DefaultLevel = new WaterSourceState(4453, 6553, 6553),
            HighLevel = new WaterSourceState(4550, 19500, 19500),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Pond,
            Position = new UnityEngine.Vector3(2557.034f, 54.75891f, 334.9223f),
            LowLevel = new WaterSourceState(3700, 24635, 24635),
            DefaultLevel = new WaterSourceState(4509, 65535, 65535),
            HighLevel = new WaterSourceState(4600, 128000, 12800),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Pond,
            Position = new UnityEngine.Vector3(1863.287f, 72.63847f, 2073.304f),
            LowLevel = new WaterSourceState(5000, 16250, 16250),
            DefaultLevel = new WaterSourceState(5400, 65535, 65535),
            HighLevel = new WaterSourceState(5625, 155000, 155000),
          },
          new PresetWaterSourceData()
          {
            SourceType = WaterSourceType.Pond,
            Position = new UnityEngine.Vector3(-1434.753f, 59.17744f, 3193.939f),
            LowLevel = new WaterSourceState(4250, 17500, 17500),
            DefaultLevel = new WaterSourceState(4494, 65535, 65535),
            HighLevel = new WaterSourceState(4650, 180500, 180500),
          },
        },
        DefaultSeaLevel = 44.09f,
      },
      #endregion

      #region Empty template to use
      //new MapPreset
      //{
      //  PresetCode = "",
      //  PresetName = "",
      //  DefaultWaterSources = new  PresetWaterSourceData[]
      //  {
      //    new PresetWaterSourceData()
      //    {
      //      SourceType = WaterSourceType.Unknown,
      //      InputPosition = new UnityEngine.Vector3(0,0,0),
      //      OutputPosition = new UnityEngine.Vector3(0,0,0),
      //      LowLevel = new WaterSourceState(0,0,0),
      //      DefaultLevel = new WaterSourceState(0,0,0),
      //      HighLevel = new WaterSourceState(0,0,0),
      //    }
      //  },
      //  DefaultSeaLevel = 0,
      //},
      #endregion
    };

    public static MapPreset GetPresetByCode(string presetCode)
    {
      return AllPresets.FirstOrDefault(p => string.Equals(p.PresetCode, presetCode, StringComparison.InvariantCultureIgnoreCase));
    }
  }
}
