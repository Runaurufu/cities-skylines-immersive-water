﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using ICities;
using Runaurufu.Utility;
using System.Xml.Serialization;
using System.Xml;

namespace Runaurufu.ImmersiveWater
{
  public class ImmersiveWaterSerializableData : SerializableDataExtensionBase
  {
    private const string KEY_INITIAL_WATER_SOURCES = "IMMERSIVE_WATER_INITIAL_WATER_SOURCES";
    private const string KEY_SELECTED_MAP_PRESET_CODE = "IMMERSIVE_WATER_SELECTED_MAP_PRESET_CODE";

    private ISerializableData serializableData;

    public override void OnCreated(ISerializableData serializableData)
    {
      //base.OnCreated(serializableData);

      this.serializableData = serializableData;
    }

    public override void OnLoadData()
    {
      //base.OnLoadData();

      try
      {
        byte[] data;

        data = serializableData.LoadData(ImmersiveWaterSerializableData.KEY_INITIAL_WATER_SOURCES);
        if (data != null && data.Length > 0)
        {
          InitialWaterSource[] initialSources = XmlDeserialize<InitialWaterSource[]>(data);
          if (initialSources != null)
            ImmersiveWaterEngine.GetInstance().InitialWaterSources = initialSources;
        }

        data = serializableData.LoadData(ImmersiveWaterSerializableData.KEY_SELECTED_MAP_PRESET_CODE);
        if (data != null && data.Length > 0)
        {
          string presetCode = XmlDeserialize<string>(data);
          if (presetCode != null)
            ImmersiveWaterEngine.GetInstance().SelectedMapPreset = MapPresets.GetPresetByCode(presetCode);
        }
      }
      catch (Exception ex)
      {
        Logger.Log("SerializableDataExtensionBase.OnLoadData() exception!" + Environment.NewLine + ex.ToString());
      }
    }

    public override void OnSaveData()
    {
      //base.OnSaveData();

      try
      {
        if (ImmersiveWaterEngine.GetInstance().InitialWaterSources != null)
        {
          byte[] data = XmlSerialize(ImmersiveWaterEngine.GetInstance().InitialWaterSources);
          if (data != null)
            serializableData.SaveData(ImmersiveWaterSerializableData.KEY_INITIAL_WATER_SOURCES, data);
        }

        if (ImmersiveWaterEngine.GetInstance().SelectedMapPreset != null)
        {
          byte[] data = XmlSerialize(ImmersiveWaterEngine.GetInstance().SelectedMapPreset.PresetCode);
          if (data != null)
            serializableData.SaveData(ImmersiveWaterSerializableData.KEY_SELECTED_MAP_PRESET_CODE, data);
        }
      }
      catch (Exception ex)
      {
        Logger.Log("SerializableDataExtensionBase.OnLoadData() exception!" + Environment.NewLine + ex.ToString());
      }
    }

    public static byte[] Serialize(object item)
    {
      BinaryFormatter binaryFormatter = new BinaryFormatter();
      using (MemoryStream memoryStream = new MemoryStream())
      {
        try
        {
          binaryFormatter.Serialize(memoryStream, item);
          memoryStream.Position = 0L;
          return memoryStream.ToArray();
        }
        catch (Exception ex)
        {
          Logger.Log("Serialize Exception:" + Environment.NewLine + ex.ToString());
        }
        return null;
      }
    }

    public static T Deserialize<T>(byte[] data) where T : class
    {
      BinaryFormatter binaryFormatter = new BinaryFormatter();
      using (MemoryStream memoryStream = new MemoryStream())
      {
        memoryStream.Write(data, 0, data.Length);
        memoryStream.Position = 0L;
        try
        {
          return binaryFormatter.Deserialize(memoryStream) as T;
        }
        catch (Exception ex)
        {
          Logger.Log("Deserialize Exception:" + Environment.NewLine + ex.ToString());
        }
        return null;
      }
    }

    public static byte[] XmlSerialize(object item)
    {
      if (item == null)
        return null;

      using (MemoryStream memoryStream = new MemoryStream())
      {
        try
        {
          XmlSerializer serializer = new XmlSerializer(item.GetType());
          using (XmlWriter xmlWriter = XmlWriter.Create(memoryStream))
          {
            serializer.Serialize(xmlWriter, item);
          }

          memoryStream.Position = 0L;
          return memoryStream.ToArray();
        }
        catch (Exception ex)
        {
          Logger.Log("XmlSerialize Exception:" + Environment.NewLine + ex.ToString());
        }
        return null;
      }
    }

    public static T XmlDeserialize<T>(byte[] data) where T : class
    {
      try
      {
        using (MemoryStream memoryStream = new MemoryStream())
        {
          memoryStream.Write(data, 0, data.Length);
          memoryStream.Position = 0L;

          XmlSerializer serializer = new XmlSerializer(typeof(T));
          using (XmlReader xmlReader = XmlReader.Create(memoryStream))
          {
            return (T)serializer.Deserialize(xmlReader);
          }
        }
      }
      catch (Exception ex)
      {
        Logger.Log("XmlDeserialize Exception:" + Environment.NewLine + ex.ToString());
        return null;
      }
    }
  }
}