﻿using System;
using ICities;
using Runaurufu.Utility;

namespace Runaurufu.ImmersiveWater
{
  public class ImmersiveWaterThreading : ThreadingExtensionBase
  {
    public override void OnCreated(IThreading threading)
    {
      base.OnCreated(threading);

      //ClimateControlEngine.GetInstance().ThreadingManager = threading;
    }

    public override void OnUpdate(float realTimeDelta, float simulationTimeDelta)
    {
      base.OnUpdate(realTimeDelta, simulationTimeDelta);

      try
      {
        ImmersiveWaterEngine iwe = ImmersiveWaterEngine.GetInstance();
        if (iwe != null)
          iwe.UpdateWater(realTimeDelta, simulationTimeDelta);
      }
      catch (Exception ex)
      {
        Logger.Log("UpdateWater() Exception: " + Environment.NewLine + ex.ToString());
      }
    }
  }
}