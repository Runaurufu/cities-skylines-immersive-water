﻿using Runaurufu.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Runaurufu.ImmersiveWater
{
  internal class MapWaterSourceData : PresetWaterSourceData
  {
    /// <summary>
    /// Index pointing to WaterManager WaterSource
    /// </summary>
    public int Index;

    public void Populate(WaterSource source)
    {
      this.Position = source.m_outputPosition;
      this.DefaultLevel = new WaterSourceState(source.m_target, source.m_inputRate, source.m_outputRate);
    }

    public void Populate(PresetWaterSourceData presetSource)
    {
      this.Position = presetSource.Position;
      this.DefaultLevel = presetSource.DefaultLevel;
      this.LowLevel = presetSource.LowLevel;
      this.HighLevel = presetSource.HighLevel;
      this.SourceType = presetSource.SourceType;
    }

    /// <summary>
    /// Populate source data with missing values ensuring all levels are set.
    /// </summary>
    public void PopulateMissingValues()
    {
      ushort terrainHeight, waterHeight, pollutionHeight;
      GeneralHelper.GetTerrainInformation(this.Position, out terrainHeight, out waterHeight, out pollutionHeight);

      float groundLevelTarget = terrainHeight;

      if (this.DefaultLevel.IsSet == false)
      {
        if (this.LowLevel.IsSet && this.HighLevel.IsSet)
        {
          this.DefaultLevel.Target = (ushort)(((float)this.LowLevel.Target + (float)this.HighLevel.Target) * 0.5f);
          this.DefaultLevel.InputRate = (uint)((this.LowLevel.InputRate + this.HighLevel.InputRate) * 0.5f);
          this.DefaultLevel.OutputRate = (uint)((this.LowLevel.OutputRate + this.HighLevel.OutputRate) * 0.5f);
        }
        else if (this.LowLevel.IsSet)
        {
          this.DefaultLevel.Target = (ushort)Mathf.Clamp(this.LowLevel.Target + 128, groundLevelTarget + 8, ushort.MaxValue);
          this.DefaultLevel.InputRate = (uint)Mathf.Clamp(this.LowLevel.InputRate * 32, 100, ushort.MaxValue * 32);
          this.DefaultLevel.OutputRate = (uint)Mathf.Clamp(this.LowLevel.OutputRate * 32, 100, ushort.MaxValue * 32);
        }
        else if (this.HighLevel.IsSet)
        {
          this.DefaultLevel.Target = (ushort)Mathf.Clamp(groundLevelTarget + (this.HighLevel.Target - groundLevelTarget) * 0.5f, groundLevelTarget + 8, ushort.MaxValue);
          this.DefaultLevel.InputRate = (uint)Mathf.Clamp(this.HighLevel.InputRate * 0.6f, 100, ushort.MaxValue * 32);
          this.DefaultLevel.OutputRate = (uint)Mathf.Clamp(this.HighLevel.OutputRate * 0.6f, 100, ushort.MaxValue * 32);
        }
        else
        {
          this.DefaultLevel.Target = (ushort)Mathf.Clamp(groundLevelTarget + 256, groundLevelTarget + 8, ushort.MaxValue);
          this.DefaultLevel.InputRate = (uint)Mathf.Clamp(groundLevelTarget * 64, 100, ushort.MaxValue * 32);
          this.DefaultLevel.OutputRate = (uint)Mathf.Clamp(groundLevelTarget * 64, 100, ushort.MaxValue * 32);
        }
      }

      if (this.LowLevel.IsSet == false)
      {
        ushort target = (ushort)Mathf.Clamp(0.5f * (float)this.DefaultLevel.Target, groundLevelTarget + 1f, ushort.MaxValue);
        uint inRrate = (uint)Mathf.Clamp(0.10f * (float)this.DefaultLevel.InputRate, 100f, ushort.MaxValue * 32f);
        uint outRate = (uint)Mathf.Clamp(0.10f * (float)this.DefaultLevel.OutputRate, 100f, ushort.MaxValue * 32f);

        this.LowLevel = new WaterSourceState(target, inRrate, outRate);
      }

      if (this.HighLevel.IsSet == false)
      {
        ushort target = (ushort)Mathf.Min((float)this.DefaultLevel.Target + ((float)this.DefaultLevel.Target - groundLevelTarget) * 1.25f, ushort.MaxValue);
        uint inRate = (uint)Mathf.Clamp(10.00f * (float)this.DefaultLevel.InputRate, 100f, ushort.MaxValue * 32);
        uint outRate = (uint)Mathf.Clamp(10.00f * (float)this.DefaultLevel.OutputRate, 100f, ushort.MaxValue * 32);

        this.HighLevel = new WaterSourceState(target, inRate, outRate);
      }
    }
  }
}
