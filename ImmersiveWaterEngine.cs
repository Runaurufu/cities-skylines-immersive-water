﻿using ColossalFramework;
using Runaurufu.Common;
using Runaurufu.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;

namespace Runaurufu.ImmersiveWater
{
  public class ImmersiveWaterEngine
  {
    private static ImmersiveWaterEngine instance;

    public static ImmersiveWaterEngine GetInstance()
    {
      if (instance == null)
        instance = new ImmersiveWaterEngine();
      return instance;
    }

    public bool IsInitialized
    {
      get; private set;
    }

    private MapPreset selectedMapPreset = null;
    public MapPreset SelectedMapPreset
    {
      get
      { return this.selectedMapPreset; }
      set
      {
        this.selectedMapPreset = value;
        this.ResetWaterSourcesToDefaults();
      }
    }

    private MapWaterSourceData[] mapWaterSources = null;

    internal MapWaterSourceData[] MapWaterSources
    {
      get { return this.mapWaterSources; }
    }

    private IClimateControlEngine climateControlEngine = null;

    public void Initialize()
    {
      this.climateControlEngine = SingletonHelper.GetSingleInstance<IClimateControlEngine>();
      if (this.climateControlEngine != null)
      {
        if (ModConfig.GetInstance().UseClimateControlWaterQuantities == true)
          this.climateControlEngine.RegisterModuleHandler(ClimateControlModuleTypeIdentifiers.WaterSource, Mod.GetInstance());
        else
          this.climateControlEngine.DeregisterModuleHandler(ClimateControlModuleTypeIdentifiers.WaterSource, Mod.GetInstance());
      }
      this.IsInitialized = true;
    }

    public void Uninitialize()
    {
      this.IsInitialized = false;
    }

    private bool HandleWaterSourceModuleOperation(TimeSpan timeDelta)
    {
      return true;
    }


    DateTime lastClimateTimeUpdate = DateTime.MinValue;

    public void UpdateWater(float realTimeDelta, float simulationTimeDelta)
    {
      if (this.IsInitialized == false)
        return;

      if (!Singleton<LoadingManager>.instance.m_loadingComplete)
        return;

      ItemClass.Availability mode = Singleton<ToolManager>.instance.m_properties.m_mode;
      if ((mode & ItemClass.Availability.Game) == ItemClass.Availability.None)
        return;

      if (this.InitialWaterSources == null)
        this.CreateInitialWaterSourcesData();

      if (this.mapWaterSources == null)
        this.ResetWaterSourcesToDefaults();

      if (ModConfig.GetInstance().WeatherAffectsWaterSources == true)
      {
        WaterSimulation waterSimulation = Singleton<TerrainManager>.instance.WaterSimulation;
        FastList<WaterSource> waterSources = waterSimulation.m_waterSources;


        lock (waterSources)
        {
          // Check water sources if we need to repopulate them
          bool needToRepopulate = false;

          foreach (MapWaterSourceData mwsd in this.mapWaterSources)
          {
            if (waterSources.m_buffer.Length <= mwsd.Index)
            {
              needToRepopulate = true;
              break;
            }
          }

          if (needToRepopulate)
          {
            Utility.Logger.DebugWarning("Need to repopulate WATER SOURCES!");
            this.ResetWaterSourcesToDefaults();
          }

          if (this.climateControlEngine != null && ModConfig.GetInstance().UseClimateControlWaterQuantities == true)
          { // use climate time
            DateTime climateTime = this.climateControlEngine.GetClimateDateTime();

            if (this.lastClimateTimeUpdate != DateTime.MinValue)
            {
              TimeSpan climateTimeDelta = climateTime - this.lastClimateTimeUpdate;

              float desiredSurfaceWater = 0.0f;
              float desiredGroundWater = 0.0f;
              float desiredOutOfMapWater = 0.0f;

              foreach (MapWaterSourceData mwsd in this.mapWaterSources)
              {
                switch (mwsd.SourceType)
                {
                  case WaterSourceType.Unknown:
                    float third = mwsd.HighLevel.OutputRate * 0.33f;
                    desiredSurfaceWater += third;
                    desiredGroundWater += third;
                    desiredOutOfMapWater += third;
                    break;
                  case WaterSourceType.River:
                    desiredOutOfMapWater += mwsd.HighLevel.OutputRate;
                    break;
                  case WaterSourceType.Spring:
                    desiredGroundWater += mwsd.HighLevel.OutputRate;
                    break;
                  case WaterSourceType.Pond:
                    desiredSurfaceWater += mwsd.HighLevel.OutputRate;
                    break;
                  case WaterSourceType.Lake:
                    desiredSurfaceWater += mwsd.HighLevel.OutputRate;
                    break;
                  case WaterSourceType.Sea:
                    // infinite sources does not affect water supplies
                    break;
                  case WaterSourceType.BodyOfWater:
                    // infinite sources does not affect water supplies
                    break;
                }
              }

              float acquiredSurfaceWater = this.climateControlEngine.Acquire(ClimateControlResourceTypeIdentifiers.SurfaceWater, desiredSurfaceWater);
              float acquiredGroundWater = this.climateControlEngine.Acquire(ClimateControlResourceTypeIdentifiers.GroundWater, desiredGroundWater);
              float acquiredOutOfMapWater = this.climateControlEngine.Acquire(ClimateControlResourceTypeIdentifiers.OutOfMapGroundWater, desiredOutOfMapWater);

              float ratioSurfaceWater = desiredSurfaceWater == 0 ? 0 : acquiredSurfaceWater / desiredSurfaceWater;
              float ratioGroundWater = desiredGroundWater == 0 ? 0 : acquiredGroundWater / desiredGroundWater;
              float ratioOutOfMapWater = desiredOutOfMapWater == 0 ? 0 : acquiredOutOfMapWater / desiredOutOfMapWater;

              float ratioUnknownWater = (ratioSurfaceWater + ratioGroundWater + ratioOutOfMapWater) * 0.33f;

              foreach (MapWaterSourceData mwsd in this.mapWaterSources)
              {
                switch (mwsd.SourceType)
                {
                  case WaterSourceType.Unknown:
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)Math.Max(mwsd.LowLevel.OutputRate, mwsd.HighLevel.OutputRate * ratioUnknownWater);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)Math.Max(mwsd.LowLevel.InputRate, mwsd.HighLevel.InputRate * ratioUnknownWater);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)Math.Max(mwsd.LowLevel.Target, mwsd.HighLevel.Target * ratioUnknownWater);
                    break;
                  case WaterSourceType.River:
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)Math.Max(mwsd.LowLevel.OutputRate, mwsd.HighLevel.OutputRate * ratioOutOfMapWater);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)Math.Max(mwsd.LowLevel.InputRate, mwsd.HighLevel.InputRate * ratioOutOfMapWater);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)Math.Max(mwsd.LowLevel.Target, mwsd.HighLevel.Target * ratioOutOfMapWater);
                    break;
                  case WaterSourceType.Spring:
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)Math.Max(mwsd.LowLevel.OutputRate, mwsd.HighLevel.OutputRate * ratioGroundWater);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)Math.Max(mwsd.LowLevel.InputRate, mwsd.HighLevel.InputRate * ratioGroundWater);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)Math.Max(mwsd.LowLevel.Target, mwsd.HighLevel.Target * ratioGroundWater);
                    break;
                  case WaterSourceType.Pond:
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)Math.Max(mwsd.LowLevel.OutputRate, mwsd.HighLevel.OutputRate * ratioSurfaceWater);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)Math.Max(mwsd.LowLevel.InputRate, mwsd.HighLevel.InputRate * ratioSurfaceWater);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)Math.Max(mwsd.LowLevel.Target, mwsd.HighLevel.Target * ratioSurfaceWater);
                    break;
                  case WaterSourceType.Lake:
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)Math.Max(mwsd.LowLevel.OutputRate, mwsd.HighLevel.OutputRate * ratioSurfaceWater);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)Math.Max(mwsd.LowLevel.InputRate, mwsd.HighLevel.InputRate * ratioSurfaceWater);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)Math.Max(mwsd.LowLevel.Target, mwsd.HighLevel.Target * ratioSurfaceWater);
                    break;
                  case WaterSourceType.Sea:
                    // infinite sources does not affect water supplies
                    waterSources.m_buffer[mwsd.Index].m_outputRate = mwsd.DefaultLevel.OutputRate;
                    waterSources.m_buffer[mwsd.Index].m_inputRate = mwsd.DefaultLevel.InputRate;
                    waterSources.m_buffer[mwsd.Index].m_target = mwsd.DefaultLevel.Target;
                    break;
                  case WaterSourceType.BodyOfWater:
                    // infinite sources does not affect water supplies
                    waterSources.m_buffer[mwsd.Index].m_outputRate = mwsd.DefaultLevel.OutputRate;
                    waterSources.m_buffer[mwsd.Index].m_inputRate = mwsd.DefaultLevel.InputRate;
                    waterSources.m_buffer[mwsd.Index].m_target = mwsd.DefaultLevel.Target;
                    break;
                }
              }

            }
            this.lastClimateTimeUpdate = climateTime;
          }
          else
          { // use simulationTimeDelta & weather properties

            WeatherManager wm = Singleton<WeatherManager>.instance;

            float unknownWater = (wm.m_groundWetness + wm.m_currentRain + wm.m_currentCloud * 0.25f + wm.m_currentFog * 0.25f) * 0.4f;
            float riverWater = 0.2f + wm.m_groundWetness * 0.7f + wm.m_currentRain * 0.1f;
            float springWater = 0.35f + wm.m_groundWetness * 0.65f;
            float pondWater = (wm.m_groundWetness + wm.m_currentRain) * 0.5f;
            float lakeWater = 0.25f + (wm.m_groundWetness + wm.m_currentRain) * 0.5f * 0.75f;

            foreach (MapWaterSourceData mwsd in this.mapWaterSources)
            {
              switch (mwsd.SourceType)
              {
                case WaterSourceType.Unknown:
                  if (unknownWater > 0.5f)
                  {
                    float rate = (unknownWater - 0.5f) * 2f;
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)(mwsd.DefaultLevel.OutputRate + (mwsd.HighLevel.OutputRate - mwsd.DefaultLevel.OutputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)(mwsd.DefaultLevel.InputRate + (mwsd.HighLevel.InputRate - mwsd.DefaultLevel.InputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)(mwsd.DefaultLevel.Target + (mwsd.HighLevel.Target - mwsd.DefaultLevel.Target) * rate);
                  }
                  else
                  {
                    float rate = unknownWater * 2f;
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)(mwsd.LowLevel.OutputRate + (mwsd.DefaultLevel.OutputRate - mwsd.LowLevel.OutputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)(mwsd.LowLevel.InputRate + (mwsd.DefaultLevel.InputRate - mwsd.LowLevel.InputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)(mwsd.LowLevel.Target + (mwsd.DefaultLevel.Target - mwsd.LowLevel.Target) * rate);
                  }
                  break;
                case WaterSourceType.River:
                  if (riverWater > 0.5f)
                  {
                    float rate = (riverWater - 0.5f) * 2f;
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)(mwsd.DefaultLevel.OutputRate + (mwsd.HighLevel.OutputRate - mwsd.DefaultLevel.OutputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)(mwsd.DefaultLevel.InputRate + (mwsd.HighLevel.InputRate - mwsd.DefaultLevel.InputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)(mwsd.DefaultLevel.Target + (mwsd.HighLevel.Target - mwsd.DefaultLevel.Target) * rate);
                  }
                  else
                  {
                    float rate = riverWater * 2f;
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)(mwsd.LowLevel.OutputRate + (mwsd.DefaultLevel.OutputRate - mwsd.LowLevel.OutputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)(mwsd.LowLevel.InputRate + (mwsd.DefaultLevel.InputRate - mwsd.LowLevel.InputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)(mwsd.LowLevel.Target + (mwsd.DefaultLevel.Target - mwsd.LowLevel.Target) * rate);
                  }
                  break;
                case WaterSourceType.Spring:
                  if (springWater > 0.5f)
                  {
                    float rate = (springWater - 0.5f) * 2f;
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)(mwsd.DefaultLevel.OutputRate + (mwsd.HighLevel.OutputRate - mwsd.DefaultLevel.OutputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)(mwsd.DefaultLevel.InputRate + (mwsd.HighLevel.InputRate - mwsd.DefaultLevel.InputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)(mwsd.DefaultLevel.Target + (mwsd.HighLevel.Target - mwsd.DefaultLevel.Target) * rate);
                  }
                  else
                  {
                    float rate = springWater * 2f;
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)(mwsd.LowLevel.OutputRate + (mwsd.DefaultLevel.OutputRate - mwsd.LowLevel.OutputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)(mwsd.LowLevel.InputRate + (mwsd.DefaultLevel.InputRate - mwsd.LowLevel.InputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)(mwsd.LowLevel.Target + (mwsd.DefaultLevel.Target - mwsd.LowLevel.Target) * rate);
                  }
                  break;
                case WaterSourceType.Pond:
                  if (pondWater > 0.5f)
                  {
                    float rate = (pondWater - 0.5f) * 2f;
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)(mwsd.DefaultLevel.OutputRate + (mwsd.HighLevel.OutputRate - mwsd.DefaultLevel.OutputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)(mwsd.DefaultLevel.InputRate + (mwsd.HighLevel.InputRate - mwsd.DefaultLevel.InputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)(mwsd.DefaultLevel.Target + (mwsd.HighLevel.Target - mwsd.DefaultLevel.Target) * rate);
                  }
                  else
                  {
                    float rate = pondWater * 2f;
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)(mwsd.LowLevel.OutputRate + (mwsd.DefaultLevel.OutputRate - mwsd.LowLevel.OutputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)(mwsd.LowLevel.InputRate + (mwsd.DefaultLevel.InputRate - mwsd.LowLevel.InputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)(mwsd.LowLevel.Target + (mwsd.DefaultLevel.Target - mwsd.LowLevel.Target) * rate);
                  }
                  break;
                case WaterSourceType.Lake:
                  if (lakeWater > 0.5f)
                  {
                    float rate = (lakeWater - 0.5f) * 2f;
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)(mwsd.DefaultLevel.OutputRate + (mwsd.HighLevel.OutputRate - mwsd.DefaultLevel.OutputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)(mwsd.DefaultLevel.InputRate + (mwsd.HighLevel.InputRate - mwsd.DefaultLevel.InputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)(mwsd.DefaultLevel.Target + (mwsd.HighLevel.Target - mwsd.DefaultLevel.Target) * rate);
                  }
                  else
                  {
                    float rate = lakeWater * 2f;
                    waterSources.m_buffer[mwsd.Index].m_outputRate = (uint)(mwsd.LowLevel.OutputRate + (mwsd.DefaultLevel.OutputRate - mwsd.LowLevel.OutputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_inputRate = (uint)(mwsd.LowLevel.InputRate + (mwsd.DefaultLevel.InputRate - mwsd.LowLevel.InputRate) * rate);
                    waterSources.m_buffer[mwsd.Index].m_target = (ushort)(mwsd.LowLevel.Target + (mwsd.DefaultLevel.Target - mwsd.LowLevel.Target) * rate);
                  }
                  break;
                case WaterSourceType.Sea:
                  waterSources.m_buffer[mwsd.Index].m_outputRate = mwsd.DefaultLevel.OutputRate;
                  waterSources.m_buffer[mwsd.Index].m_inputRate = mwsd.DefaultLevel.InputRate;
                  waterSources.m_buffer[mwsd.Index].m_target = mwsd.DefaultLevel.Target;
                  break;
                case WaterSourceType.BodyOfWater:
                  waterSources.m_buffer[mwsd.Index].m_outputRate = mwsd.DefaultLevel.OutputRate;
                  waterSources.m_buffer[mwsd.Index].m_inputRate = mwsd.DefaultLevel.InputRate;
                  waterSources.m_buffer[mwsd.Index].m_target = mwsd.DefaultLevel.Target;
                  break;
              }
            }
          }
        }
      }

      //bool pressedDown = UnityEngine.Input.GetKeyDown(UnityEngine.KeyCode.O);
      //if (pressedDown)
      //{
      //  GameObject gameObject = GameObject.FindGameObjectWithTag("MainCamera");
      //  if (gameObject != null)
      //  {
      //    Vector3 worldPos;
      //    if (GeneralHelper.GetTerrainPositionFromMouse(out worldPos))
      //    {
      //      WaterSource s = new WaterSource();
      //      s.m_inputPosition = worldPos;
      //      s.m_outputPosition = worldPos;
      //      s.m_inputRate = 0;
      //      s.m_outputRate = 500;
      //      s.m_target = (ushort)(worldPos.y + 10);
      //      s.m_type = 1;
      //      ushort wsid;
      //      if (Singleton<TerrainManager>.instance.WaterSimulation.CreateWaterSource(out wsid, s))
      //      {
      //        ;
      //      }
      //    }
      //  }
      //}

      //if (UnityEngine.Input.GetKey(UnityEngine.KeyCode.P))
      //{
      //  Vector3 worldPos;
      //  if (GeneralHelper.GetTerrainPositionFromMouse(out worldPos))
      //  {
      //    GeneralHelper.TryAddWater(worldPos, 1000);
      //  }
      //}
    }

    public void ResetWaterSourcesToDefaults()
    {
      if (ModConfig.GetInstance().DynamicWaterSourcesPlacement)
      {
        this.PrepareDynamicWaterSources();
      }
      else
      {
        if (this.SelectedMapPreset == null)
          this.ResetWaterSourcesToDefaultsFromInitial();
        else
        {
          if (ModConfig.GetInstance().SpawnExtraCreeks && this.SelectedMapPreset.ExtendedWaterSources != null)
          {
            this.ResetWaterSourcesToDefaults(this.SelectedMapPreset.ExtendedWaterSources);
          }
          else
          {
            this.ResetWaterSourcesToDefaults(this.SelectedMapPreset.DefaultWaterSources);
          }
        }
      }

      // when changing map preset also sea level should accommodate!
      if(this.SelectedMapPreset != null)
      {
        TerrainManager.instance.WaterSimulation.m_currentSeaLevel = this.SelectedMapPreset.DefaultSeaLevel;
        TerrainManager.instance.WaterSimulation.m_nextSeaLevel = this.SelectedMapPreset.DefaultSeaLevel;
      }
    }

    /// <summary>
    /// Remove all TYPE_NATURAL water sources.
    /// </summary>
    private void RemoveAllWaterSources()
    {
      FastList<WaterSource> waterSources = Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources;

      lock (waterSources)
      {
        for (int i = waterSources.m_size - 1; i >= 0; --i)
        {
          if (waterSources.m_buffer[i].m_type == WaterSource.TYPE_NATURAL)
            waterSources.m_buffer[i].m_type = WaterSource.TYPE_NONE;
          waterSources.RemoveAt(i);
        }
      }
      //this.terrainManager.WaterSimulation.ReleaseWaterSource(this.waterSources[id in waterSources fastlist]);
    }

    internal InitialWaterSource[] InitialWaterSources = null;

    private void CreateInitialWaterSourcesData()
    {
      List<InitialWaterSource> initialWaterSources = new List<InitialWaterSource>();

      FastList<WaterSource> waterSources = Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources;
      for (int i = 0; i < waterSources.m_size; i++)
      {
        if (waterSources.m_buffer[i].m_type == WaterSource.TYPE_NATURAL)
        {
          InitialWaterSource iws = new InitialWaterSource(waterSources.m_buffer[i]);
          initialWaterSources.Add(iws);
        }
      }

      this.InitialWaterSources = initialWaterSources.ToArray();
    }

    private void ResetWaterSourcesToDefaultsFromInitial()
    {
      if (this.InitialWaterSources == null)
      {
        // do not reset water sources if initials are not available.
        Utility.Logger.DebugWarning("Initial water sources not found!");
        return;
      }
      this.RemoveAllWaterSources();

      List<MapWaterSourceData> mapSources = new List<MapWaterSourceData>();

      foreach (InitialWaterSource iws in this.InitialWaterSources)
      {
        WaterSource s = new WaterSource();
        s.m_inputPosition = iws.InputPosition;
        s.m_outputPosition = iws.OutputPosition;
        s.m_inputRate = iws.InputRate;
        s.m_outputRate = iws.OutputRate;
        s.m_target = iws.Target;
        s.m_type = WaterSource.TYPE_NATURAL;
        ushort wsid;
        if (Singleton<TerrainManager>.instance.WaterSimulation.CreateWaterSource(out wsid, s) == false)
        {
          Utility.Logger.DebugWarning("Could not create water source!");
        }
        else
        {
          MapWaterSourceData mwsd = new MapWaterSourceData();
          mwsd.Populate(s);
          mwsd.Index = wsid - 1;
          mwsd.SourceType = WaterSourceType.Unknown;
          mwsd.PopulateMissingValues();
          mapSources.Add(mwsd);
        }
      }

      this.mapWaterSources = mapSources.ToArray();
    }

    private void ResetWaterSourcesToDefaults(PresetWaterSourceData[] sources)
    {
      if (this.SelectedMapPreset == null)
        return; // do not reset water sources if preset is not set.
      this.RemoveAllWaterSources();

      List<MapWaterSourceData> mapSources = new List<MapWaterSourceData>();

      foreach (PresetWaterSourceData pwsd in sources)
      {
        WaterSource s = new WaterSource();
        s.m_inputPosition = pwsd.Position;
        s.m_outputPosition = pwsd.Position;
        s.m_inputRate = pwsd.DefaultLevel.InputRate;
        s.m_outputRate = pwsd.DefaultLevel.OutputRate;
        s.m_target = pwsd.DefaultLevel.Target;
        s.m_type = WaterSource.TYPE_NATURAL;
        ushort wsid;
        if (Singleton<TerrainManager>.instance.WaterSimulation.CreateWaterSource(out wsid, s) == false)
        {
          Utility.Logger.DebugWarning("Could not create water source!");
        }
        else
        {
          MapWaterSourceData mwsd = new MapWaterSourceData();
          mwsd.Populate(pwsd);
          mwsd.Index = wsid - 1;
          mwsd.PopulateMissingValues();
          mapSources.Add(mwsd);
        }
      }

      this.mapWaterSources = mapSources.ToArray();
    }

    public void PrepareDynamicWaterSources()
    {
      this.RemoveAllWaterSources();

      // TODO
    }


    //public DefaultWaterSourceData[] DefaultMapWaterSources { get; set; }

    //private void CreateDefaultMapWaterSources()
    //{
    //  if (this.DefaultMapWaterSources == null)
    //  {
    //    FastList<WaterSource> waterSources = Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources;
    //    List<DefaultWaterSourceData> mapSourcesList = new List<DefaultWaterSourceData>();

    //    for (int i = 0; i < waterSources.m_size; i++)
    //    {
    //      if (waterSources.m_buffer[i].m_type == WaterSource.TYPE_NATURAL && waterSources.m_buffer[i].m_target > 0)
    //      {
    //        mapSourcesList.Add(new DefaultWaterSourceData()
    //        {
    //          Index = i,
    //          Target = waterSources.m_buffer[i].m_target,
    //          InputRate = waterSources.m_buffer[i].m_inputRate,
    //          OutputRate = waterSources.m_buffer[i].m_outputRate,
    //        });
    //      }
    //    }

    //    this.DefaultMapWaterSources = mapSourcesList.ToArray();
    //  }
    //}

    //private void HandlePrecipitationAlterWaterSources(float yearProgress, bool firstUpdateThisYear, TimeSpan timeDelta)
    //{
    //  if (this.ClimateGlobals == null)
    //    return;

    //  this.CreateDefaultMapWaterSources();

    //  FastList<WaterSource> waterSources = Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources;
    //  if (mapSources == null)
    //  {
    //    mapSources = new MapWaterSource[this.DefaultMapWaterSources.Length];

    //    for (int i = 0; i < mapSources.Length; i++)
    //    {
    //      MapWaterSource source = new MapWaterSource();
    //      source.Index = this.DefaultMapWaterSources[i].Index;
    //      source.MinTarget = (ushort)Mathf.Clamp(0.5f * this.DefaultMapWaterSources[i].Target, 63.99f * waterSources.m_buffer[source.Index].m_outputPosition.y + 8, ushort.MaxValue);
    //      source.MaxTarget = (ushort)Mathf.Min(this.DefaultMapWaterSources[i].Target + (this.DefaultMapWaterSources[i].Target - 63.99f * waterSources.m_buffer[source.Index].m_outputPosition.y) * 1.25f, ushort.MaxValue);
    //      source.MinOutputRate = waterSources.m_buffer[source.Index].m_outputRate = (uint)Mathf.Clamp(0.10f * this.DefaultMapWaterSources[i].OutputRate, 100, ushort.MaxValue);
    //      source.MaxOutputRate = waterSources.m_buffer[source.Index].m_outputRate = (uint)Mathf.Clamp(10.00f * this.DefaultMapWaterSources[i].OutputRate, 100, ushort.MaxValue);

    //      source.YearlyExpectedFlow = 366 * 24 * this.DefaultMapWaterSources[i].OutputRate;
    //      source.CurrentYearFlow = (ulong)(source.YearlyExpectedFlow * yearProgress);

    //      // input must be reduced to reduce rivers "sucking" water upstream.
    //      waterSources.m_buffer[source.Index].m_inputRate = (uint)(this.DefaultMapWaterSources[i].InputRate * 0.0001f);

    //      mapSources[i] = source;
    //    }

    //    firstUpdateThisYear = false;
    //  }

    //  for (int i = 0; i < mapSources.Length; i++)
    //  {
    //    MapWaterSource source = mapSources[i];

    //    // update current flow!
    //    if (firstUpdateThisYear)
    //      source.CurrentYearFlow = 0;
    //    source.CurrentYearFlow += (ulong)(waterSources.m_buffer[source.Index].m_flow * timeDelta.TotalHours);

    //    if (this.ClimateGlobals.PrecipitationHourlyAverage == 0)
    //    {
    //      waterSources.m_buffer[source.Index].m_target = this.DefaultMapWaterSources[i].Target;
    //      waterSources.m_buffer[source.Index].m_outputRate = this.DefaultMapWaterSources[i].OutputRate;
    //    }
    //    else
    //    {
    //      bool tooMuchFlow = source.CurrentYearFlow > source.YearlyExpectedFlow * 1.5f;

    //      float hourlyFall = (this.CurrentRain * 4.00f /*+ this.GroundWetness * 0.05f*/) / (float)timeDelta.TotalHours;
    //      float ratio = hourlyFall / this.ClimateGlobals.PrecipitationHourlyAverage;

    //      float goalTarget;
    //      float goalOutputRate;

    //      float progress;

    //      if (ratio > 1.2f)
    //      {
    //        goalTarget = source.MaxTarget;
    //        goalOutputRate = source.MaxOutputRate;

    //        progress = 0.0003f;

    //        if (waterSources.m_buffer[source.Index].m_target < this.DefaultMapWaterSources[i].Target)
    //          progress *= 3;

    //        if (tooMuchFlow)
    //          progress *= 0.1f;
    //      }
    //      else if (ratio > 0.8f)
    //      {
    //        goalTarget = this.DefaultMapWaterSources[i].Target;
    //        goalOutputRate = this.DefaultMapWaterSources[i].OutputRate;

    //        progress = 0.0050f;

    //        if (waterSources.m_buffer[source.Index].m_target > this.DefaultMapWaterSources[i].Target && tooMuchFlow)
    //          progress *= 5f;
    //      }
    //      else
    //      {
    //        goalTarget = source.MinTarget;
    //        goalOutputRate = source.MinOutputRate;

    //        progress = 0.0003f;

    //        if (waterSources.m_buffer[source.Index].m_target > this.DefaultMapWaterSources[i].Target)
    //          progress *= 3;

    //        if (tooMuchFlow)
    //          progress *= 2f;
    //      }
    //      waterSources.m_buffer[source.Index].m_target = (ushort)Mathf.Lerp(waterSources.m_buffer[source.Index].m_target, goalTarget, progress);
    //      waterSources.m_buffer[source.Index].m_outputRate = (uint)Mathf.Lerp(waterSources.m_buffer[source.Index].m_outputRate, goalOutputRate, progress);
    //    }
    //  }
    //}
  }

  [Serializable]
  public class InitialWaterSource
  {
    public UnityEngine.Vector3 InputPosition;
    public UnityEngine.Vector3 OutputPosition;
    public ushort Target;
    public uint InputRate;
    public uint OutputRate;

    public InitialWaterSource()
    {

    }

    public InitialWaterSource(WaterSource ws)
    {
      this.InputPosition = ws.m_inputPosition;
      this.OutputPosition = ws.m_outputPosition;
      this.Target = ws.m_target;
      this.InputRate = ws.m_inputRate;
      this.OutputRate = ws.m_outputRate;
    }
  }
}
