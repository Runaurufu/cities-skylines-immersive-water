﻿using ColossalFramework;
using ColossalFramework.UI;
using Runaurufu.Common;
using Runaurufu.Utility;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Runaurufu.ImmersiveWater.UI
{
  internal class SelectPresetUI : UIPanelAdv
  {
    private UILabel label;

    private UIDropDown dropDown;

    private UIButton selectPresetButton;
    private UIButton noPresetSelectedButton;

    public override void Awake()
    {
      this.size = new UnityEngine.Vector2(360f, 950f);
      this.anchor = UIAnchorStyle.Bottom;
      this.backgroundSprite = "ButtonMenu";
      this.autoLayoutPadding = new RectOffset(10, 10, 2, 2);
      this.padding = new RectOffset(0, 0, 0, 5);
      this.autoLayout = true;
      this.autoFitChildrenVertically = true;
      this.autoLayoutDirection = LayoutDirection.Vertical;

      this.label = this.AddUIComponent<UILabel>();
      this.label.text = "Select Map Preset";
      this.label.textAlignment = UIHorizontalAlignment.Center;

      this.dropDown = UIFactory.CreateDropDown(this);

      this.dropDown.items = MapPresets.AllPresets.Select(s => s.PresetName).ToArray();
      this.dropDown.autoListWidth = true;
      //this.dropDown.selectedIndex // try to find preset using found water sources :o

      this.selectPresetButton = UIFactory.CreateButton(this);
      this.selectPresetButton.text = "Use Selected Preset";
      this.selectPresetButton.textHorizontalAlignment = UIHorizontalAlignment.Center;
      this.selectPresetButton.textVerticalAlignment = UIVerticalAlignment.Middle;
      this.selectPresetButton.eventClick += SetPresetButton_eventClick;

      this.noPresetSelectedButton = UIFactory.CreateButton(this);
      this.noPresetSelectedButton.text = "Do Not Use Preset";
      this.noPresetSelectedButton.textHorizontalAlignment = UIHorizontalAlignment.Center;
      this.noPresetSelectedButton.textVerticalAlignment = UIVerticalAlignment.Middle;
      this.noPresetSelectedButton.eventClick += NoPresetSelectedButton_eventClick;

      base.Awake();
    }

    private void NoPresetSelectedButton_eventClick(UIComponent component, UIMouseEventParameter eventParam)
    {
      ImmersiveWaterEngine.GetInstance().SelectedMapPreset = null;
      this.Hide();
      this.Disable();
    }

    private void DropDown_eventSelectedIndexChanged(UIComponent component, int value)
    {
      // ??
    }

    private void SetPresetButton_eventClick(UIComponent component, UIMouseEventParameter eventParam)
    {
      ImmersiveWaterEngine.GetInstance().SelectedMapPreset = MapPresets.AllPresets[this.dropDown.selectedIndex];
      this.Hide();
      this.Disable();
    }

    public new void Update()
    {
      //if (this.IsUpdateAllowed)
      //{
      //}
      base.Update();
    }
  }
}