﻿using ColossalFramework;
using ColossalFramework.UI;
using Runaurufu.Utility;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace Runaurufu.ImmersiveWater.UI
{
  internal class SourcesUI : UIPanelAdv
  {
    private UIButton reloadSourcesButton;

    private UILabel sourceNumberLabel;
    private UISliderAdv sourceSlider;

    private UILabel positionLabel;

    private UIButton goToPositionButton;

    private UILabel targetLabel;
    private UISliderAdv targetSlider;

    private UIButton setTargetToSeaLevelButton;

    private UILabel inputLabel;
    private UISliderAdv inputSlider;

    private UILabel outputLabel;
    private UISliderAdv outputSlider;

    private UILabel lowLevelLabel;
    private UILabel defaultLevelLabel;
    private UILabel highLevelLabel;

    private UILabel scenarionLabel;
    private UILabel mapLabel;
    private UILabel environmentLabel;

    private UIButton savePresetButton;


    public override void Awake()
    {
      this.size = new UnityEngine.Vector2(560f, 950f);
      this.anchor = UIAnchorStyle.Bottom;
      this.backgroundSprite = "ButtonMenu";
      this.autoLayoutPadding = new RectOffset(10, 10, 2, 2);
      this.padding = new RectOffset(0, 0, 0, 5);
      this.autoLayout = true;
      this.autoFitChildrenVertically = true;
      this.autoLayoutDirection = LayoutDirection.Vertical;

      //this.reloadSourcesButton = UIFactory.CreateButton(this);
      //this.reloadSourcesButton.text = "Reload sources";
      //this.reloadSourcesButton.eventClick += ReloadSourcesButton_eventClick;

      this.sourceNumberLabel = this.AddUIComponent<UILabel>();
      this.sourceSlider = UIFactory.CreateSlider(this, 0, 100);
      this.sourceSlider.stepSize = 1;
      this.sourceSlider.eventValueChanged += SourceSlider_eventValueChanged;

      this.positionLabel = this.AddUIComponent<UILabel>();

      this.goToPositionButton = UIFactory.CreateButton(this);
      this.goToPositionButton.text = "Go to source";
      this.goToPositionButton.eventClick += GoToPositionButton_eventClick;

      this.targetLabel = this.AddUIComponent<UILabel>();
      this.targetSlider = UIFactory.CreateSlider(this, 0, ushort.MaxValue);
      this.targetSlider.stepSize = 1;
      this.targetSlider.eventValueChanged += TargetSlider_eventValueChanged;

      this.setTargetToSeaLevelButton = UIFactory.CreateButton(this);
      this.setTargetToSeaLevelButton.text = "Set to sea level";
      this.setTargetToSeaLevelButton.eventClick += SetTargetToSeaLevelButton_eventClick;

      this.inputLabel = this.AddUIComponent<UILabel>();
      this.inputSlider = UIFactory.CreateSlider(this, 0, ushort.MaxValue * 8);
      this.inputSlider.stepSize = 1;
      this.inputSlider.eventValueChanged += InputSlider_eventValueChanged;

      this.outputLabel = this.AddUIComponent<UILabel>();
      this.outputSlider = UIFactory.CreateSlider(this, 0, ushort.MaxValue * 8);
      this.outputSlider.stepSize = 1;
      this.outputSlider.eventValueChanged += OutputSlider_eventValueChanged;

      this.lowLevelLabel = this.AddUIComponent<UILabel>();
      this.defaultLevelLabel = this.AddUIComponent<UILabel>();
      this.highLevelLabel = this.AddUIComponent<UILabel>();


      this.scenarionLabel = this.AddUIComponent<UILabel>();
      this.mapLabel = this.AddUIComponent<UILabel>();
      this.environmentLabel = this.AddUIComponent<UILabel>();

      this.savePresetButton = UIFactory.CreateButton(this);
      this.savePresetButton.text = "Save preset";
      this.savePresetButton.eventClick += SavePresetButton_eventClick;

      base.Awake();
    }

    private void SetTargetToSeaLevelButton_eventClick(UIComponent component, UIMouseEventParameter eventParam)
    {
      if (ImmersiveWaterEngine.GetInstance().MapWaterSources == null)
        return;

      int index = this.CurrentlySelectedIndex;
      if (index < 0)
        return;

      float seaLevel = Singleton<TerrainManager>.instance.WaterSimulation.m_currentSeaLevel;


      Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_buffer[index].m_target = (ushort)(seaLevel * 64);
    }

    //private int[] indexes = null;

    //private void ReloadSourcesButton_eventClick(UIComponent component, UIMouseEventParameter eventParam)
    //{
    //  this.indexes = null;
    //  if (Singleton<TerrainManager>.instance != null)
    //  {
    //    FastList<WaterSource> waterSources = Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources;
    //    List<int> waterSourcesIndexes = new List<int>();

    //    for (int i = 0; i < waterSources.m_size; i++)
    //    {
    //      if (waterSources.m_buffer[i].m_type == WaterSource.TYPE_NATURAL)
    //      {
    //        waterSourcesIndexes.Add(i);
    //      }
    //    }

    //    this.indexes = waterSourcesIndexes.ToArray();

    //    this.sourceSlider.minValue = 0;
    //    this.sourceSlider.maxValue = this.indexes.Length - 1;
    //    this.sourceSlider.value = 0;
    //  }
    //}

    private int CurrentlySelectedIndex
    {
      get
      {
        if (ImmersiveWaterEngine.GetInstance().MapWaterSources == null)
          return -1;
        int index = (int)this.sourceSlider.value;
        if (index >= 0 && index < ImmersiveWaterEngine.GetInstance().MapWaterSources.Length)
        {
          if (ImmersiveWaterEngine.GetInstance().MapWaterSources[index].Index < 0 || ImmersiveWaterEngine.GetInstance().MapWaterSources[index].Index >= Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_size)
            return -1;
          return index;
        }
        return -1;
      }
    }

    private void SavePresetButton_eventClick(UIComponent component, UIMouseEventParameter eventParam)
    {
      MapPreset preset = new MapPreset();
      preset.PopulateFromCurrentGameState();
      preset.Save();
    }

    private void GoToPositionButton_eventClick1(UIComponent component, UIMouseEventParameter eventParam)
    {

    }

    private void OutputSlider_eventValueChanged(UIComponent component, float value)
    {
      int index = this.CurrentlySelectedIndex;
      if (index < 0)
        return;

      int realIndex = ImmersiveWaterEngine.GetInstance().MapWaterSources[index].Index;

      Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_buffer[realIndex].m_outputRate = (uint)value;
    }

    private void InputSlider_eventValueChanged(UIComponent component, float value)
    {
      int index = this.CurrentlySelectedIndex;
      if (index < 0)
        return;

      int realIndex = ImmersiveWaterEngine.GetInstance().MapWaterSources[index].Index;

      Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_buffer[realIndex].m_inputRate = (uint)value;
    }

    private void TargetSlider_eventValueChanged(UIComponent component, float value)
    {
      int index = this.CurrentlySelectedIndex;
      if (index < 0)
        return;

      int realIndex = ImmersiveWaterEngine.GetInstance().MapWaterSources[index].Index;

      Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_buffer[realIndex].m_target = (ushort)value;
    }

    private void GoToPositionButton_eventClick(UIComponent component, UIMouseEventParameter eventParam)
    {
      int index = this.CurrentlySelectedIndex;
      if (index < 0)
        return;

      int realIndex = ImmersiveWaterEngine.GetInstance().MapWaterSources[index].Index;

      Vector3 position = Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_buffer[realIndex].m_inputPosition;

      WeatherManager.instance.QueueLightningStrike(0, position, Quaternion.identity, null);

      GeneralHelper.MoveCameraToPosition(position);
    }

    private void SourceSlider_eventValueChanged(UIComponent component, float value)
    {
      // should I do something here?
    }

    public new void Update()
    {
      if (this.IsUpdateAllowed)
      {
        if (ImmersiveWaterEngine.GetInstance().MapWaterSources != null)
        {
          int maxIndex = ImmersiveWaterEngine.GetInstance().MapWaterSources.Length - 1;

          if (this.sourceSlider.maxValue != maxIndex)
          {
            this.sourceSlider.minValue = 0;
            this.sourceSlider.maxValue = maxIndex;
            this.sourceSlider.value = 0;
          }
        }

        if (Singleton<TerrainManager>.instance != null)
        {
          if (ImmersiveWaterEngine.GetInstance().MapWaterSources != null)
          {
            int index = this.CurrentlySelectedIndex;
            if (index >= 0)
            {
              MapWaterSourceData mwsd = ImmersiveWaterEngine.GetInstance().MapWaterSources[index];
              int realIndex = mwsd.Index;

              this.sourceNumberLabel.text = "Source index: " + this.sourceSlider.value + "/" + (ImmersiveWaterEngine.GetInstance().MapWaterSources.Length - 1);
              Vector3 position = Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_buffer[realIndex].m_inputPosition;
              this.positionLabel.text = "Position: " + position.ToString();

              this.targetLabel.text = "Target: " + Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_buffer[realIndex].m_target.ToString();
              this.targetSlider.value = Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_buffer[realIndex].m_target;

              this.inputLabel.text = "Input: " + Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_buffer[realIndex].m_inputRate.ToString();
              this.inputSlider.value = Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_buffer[realIndex].m_inputRate;

              this.outputLabel.text = "Output: " + Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_buffer[realIndex].m_outputRate.ToString();
              this.outputSlider.value = Singleton<TerrainManager>.instance.WaterSimulation.m_waterSources.m_buffer[realIndex].m_outputRate;


              this.lowLevelLabel.text = mwsd.LowLevel.ToString();
              this.defaultLevelLabel.text = mwsd.DefaultLevel.ToString();
              this.highLevelLabel.text = mwsd.HighLevel.ToString();
            }
          }
        }

        try
        {
          Vector3 terPos;
          if (GeneralHelper.GetTerrainPositionFromMouse(out terPos))
          {
            ushort th, wh, ph;


            GeneralHelper.GetTerrainInformation(terPos, out th, out wh, out ph);

            this.scenarionLabel.text = "GH -> TH: " + th + " WH: " + wh + " PH: " + ph;

            float tH, wH;
            Vector3 vel, norm;


            if (TerrainManager.instance.SampleWaterData(new Vector2(terPos.x, terPos.z), out tH, out wH, out vel, out norm))
            {

              this.mapLabel.text = "TM -> TH: " + tH + " WH: " + wH;
            }

            this.environmentLabel.text = string.Format("X: {0} Y: {1} Z: {2}", terPos.x, terPos.y, terPos.z);
          }
        }
        catch (Exception ex)
        {
          Utility.Logger.Log("SourcesUI() Exception: " + Environment.NewLine + ex.ToString());
        }

        //if (Singleton<SimulationManager>.instance != null)
        //{
        //  //this.scenarionLabel.text = Singleton<SimulationManager>.instance.m_metaData.m_MapThemeMetaData != null ? "NOT NULL" : "NULL";// Singleton<SimulationManager>.instance.m_metaData.mapThemeRef;
        //  // this.mapLabel.text = "";// Singleton<SimulationManager>.instance.m_metaData.m_MapThemeMetaData.name; //Singleton<SimulationManager>.instance.m_metaData.m_CityName;
        //  this.environmentLabel.text = Singleton<SimulationManager>.instance.m_metaData.m_environment;
        //}
      }
      base.Update();
    }
  }
}