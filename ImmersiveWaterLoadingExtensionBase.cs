﻿using ColossalFramework;
using ColossalFramework.UI;
using ICities;
using Runaurufu.ImmersiveWater.UI;
using Runaurufu.Utility;
using UnityEngine;

namespace Runaurufu.ImmersiveWater
{
  public class ImmersiveWaterLoading : LoadingExtensionBase
  {
    public void CreateControlPanels()
    {
      //SourcesUI s = UIView.GetAView().AddUIComponent(typeof(SourcesUI)) as SourcesUI;
      //s.Show();

      // If engine does not have selected preset then UI for preset selection must be shown!
      if (ImmersiveWaterEngine.GetInstance().SelectedMapPreset == null)
      {
        UIView.GetAView().AddUIComponent(typeof(SelectPresetUI)).Show(true);
      }
    }

    public override void OnLevelLoaded(LoadMode mode)
    {
      base.OnLevelLoaded(mode);

      if (mode != LoadMode.LoadGame && mode != LoadMode.NewGame)
        return;

      //Logger.Log(this.loadingManager.currentTheme);
      //this.loadingManager.currentTheme = "Tropical";

      //WeatherManager wm = Singleton<WeatherManager>.instance;
      //if (wm == null)
      //  return;

      ModConfig.LoadConfig();

      ImmersiveWaterEngine.GetInstance().Initialize();

      this.CreateControlPanels();
    }

    public override void OnLevelUnloading()
    {
      base.OnLevelUnloading();

      if (ImmersiveWaterEngine.GetInstance().IsInitialized)
        ImmersiveWaterEngine.GetInstance().Uninitialize();
    }
  }
}